#include "Client.h"
#include <thread>
#include <functional>
#pragma comment(lib, "Ws2_32.lib")
int main(int argc, char* argv[]) {
	Client client;
	client.makeConnection();
	client.startTimer();
	std::thread catchMouseCoord(&Client::startTimer, std::ref(client));
	std::thread catchMouseClick(&Client::waitForClick, std::ref(client));
	catchMouseCoord.join();
	catchMouseClick.join();
	return 0;
}


