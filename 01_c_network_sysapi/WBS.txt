Planning 				 (6 hours)

Requirements analysis/elaboration 		(1 hour)

Documentation prep (wbs, modules diagram)	(3 hours)

Research (APIs, libraries, etc.) 		(2 hours)


Developing				 (30 hours)

Windows modules 			   (10 hours)

Connection module implementation		(5 hours)

Directory interaction module implementation	(5 hours)


General modules				   (10 hours)

Main modules implementation		    	(4 hours)

Client module implementation		  (2 hours)

Server module implementation		  (2 hours)

"make" file formation				(2 hours)






Testing					 (1 hours)

Windows testing					(1 hour)
